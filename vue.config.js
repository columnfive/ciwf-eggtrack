module.exports = {
  publicPath: process.env.VUE_APP_ASSETS_PATH || '',

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  }
}
