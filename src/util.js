import Papa from 'papaparse'
import axios from 'axios'
import * as iso from 'iso-3166-1'
import _ from 'lodash'
import slugify from 'slugify'
import sassVars from './assets/styles/_vars.scss'

/**
 * Root URL to the logos directory
 * @type {String}
 */
// const LOGO_ROOT = 'img/logos/'

/**
 * The list of regions in scope
 * @type {Array}
 */
export const REGION_OPTIONS = [
  { name: 'global', label: 'Global companies', icon: 'global-companies' },
  { name: 'us only', label: 'U.S. only', icon: 'united-states' },
  { name: 'eur only', label: 'EUR only', icon: 'europe' }
]

/**
 * Possible sort options
 * @type {Array}
 */
export const SORT_OPTIONS = [
  'Most Progress',
  'Least Progress'
]

/**
 * Possible company size options
 * @type {Array}
 */
export const COMPANY_SIZE_OPTIONS = [
  'All Sizes',
  'Small',
  'Medium',
  'Large',
  'X Large'
]

/**
 * Maps status badge text to the coresponding icon
 * @type {Object}
 */
export const STATUS_BADGES = {
  '100% cage free': 'badge-cage-free',
  '100% cage-free': 'badge-cage-free',
  '100% progress': 'badge-cage-free',
  'good progress': 'badge-good-progress',
  'slow progress': 'badge-slow-progress',
  'missed target': 'badge-missed-target',
  'missed commitment': 'badge-missed-target',
  'not reporting': 'badge-not-reporting',
  'no commitment': 'badge-no-commitment',
  'no public commitment': 'badge-no-commitment',
  'retracted commitment': 'badge-retracted-commitment',
  default: 'https://via.placeholder.com/50/FF00FF'
}

export const getStatusBadgeName = badgeText => {
  let t = badgeText.toLowerCase()
  return t in STATUS_BADGES ? STATUS_BADGES[t] : STATUS_BADGES.default
}

export const getBadgeColor = badge => {
  const badgeId = getStatusBadgeName(badge).replace(/-/g, '_')
  if(badgeId in sassVars) {
    return sassVars[badge]
  }
  return '#ff00ff'
}

/**
 * Contains mapping of various text labels to their icons
 * @type {Object}
 */
export const SECTOR_ICONS = {
  "Restaurants": "restaurants",
  "Manufacturers": "manufacturers",
  "Producers": "producers",
  "Retailers & Meal Kits": "retailers-mealkits",
  "Food Service & Hospitality": "food-service-hospitality"
}

/**
 * Returns the sector icon name given the sector display name
 * @param  {String} sectorName Sector display name
 * @return {String}            Icon name
 */
export const getSectorIcon = sectorName => {
  return SECTOR_ICONS[sectorName]
}

/**
 * Returns the region icon name given the region data key name
 * @param  {String} regionName Region data key name
 * @return {String}            Icon name
 */
export const getRegionIcon = regionName => {
  const region = REGION_OPTIONS.find(r => r.name === regionName)
  if(region) {
    return region.icon
  }
  return ''
}

/**
 * Egg type data
 * @type {Array}
 */
export const EGG_TYPES = [
  { name: 'Shell', abbrev: 'SH', showInLegend: true },
  { name: 'Egg Product', abbrev: 'EP', showInLegend: true },
  { name: 'Egg Product (Processed)', abbrev: 'EP', showInLegend: false },
  { name: 'Egg Product (Liquid)', abbrev: 'EP', showInLegend: false },
  { name: 'Combined', abbrev: 'CO', showInLegend: true },
  { name: 'Unspecified', abbrev: 'UN', showInLegend: true }
]

/**
 * Progress type data
 * @type {Array}
 */
export const PROGRESS_TYPES = [
  { name: 'Not Reporting', abbrev: 'NR' },
  { name: 'No Commitment', abbrev: 'NC' },
  { name: 'Not Applicable', abbrev: 'NA' }
]

/**
 * Returns an egg abbreviation for a given name value
 * @param  {String} name Name of the egg type
 * @return {String}      Egg abbreviation
 */
export const eggAbbrevFromName = name => {
  const type = EGG_TYPES.find(e => e.name === name)
  if (type) {
    return type.abbrev
  }
  return name
}

const primaryRegions = ['Global','US','EUR']
/**
 * Converts a ISO-3166-1 alpha3 string to its corresponding country name
 * @param  {String} alpha3 ISO-3166-1 alpha 3 code
 * @return {String}        Country name
 */
export const alpha3ToCountryName = alpha3 => {
  const country = iso.whereAlpha3(alpha3.trim());
  if(country) {
    return country.country
  }
  if(primaryRegions.indexOf(alpha3.trim()) !== -1) {
    return alpha3.trim()
  }
  if(!country && primaryRegions.indexOf(alpha3.trim()) === -1) {
    // Check for hard code swaps
    switch(alpha3.trim().toLowerCase()) {
      case 'uk': return 'United Kingdom'
    }
  }
  console.warn(`Unable to find country for ISO code ${alpha3}`)
  return alpha3.trim()
}

/**
 * Converts a country name to its corresponding ISO-3166-1 alpha 3 code
 * @param  {String} countryName Country name
 * @return {String}             ISO-3166-1 alpha 3
 */
export const countryNameToAlpha3 = countryName => {
  const country = iso.whereCountry(countryName.trim())
  if(country) {
    return country.alpha3
  }
  if(primaryRegions.indexOf(countryName.trim()) !== -1) {
    return countryName
  }
  if(!country && primaryRegions.indexOf(countryName.trim()) === -1) {
    // Check for hard code swaps
    switch(countryName.trim().toLowerCase()) {
      case 'united kingdom': return 'UK'
      case 'russia': return 'RUS'
      case 'ireland and northern ireland': return 'IRA'
      case 'northern ireland': return 'N-IRL'
    }
  }
  console.warn(`Unable to find ISO code for country ${countryName}`)
  return countryName.trim()
}

/**
 * Regex used to extract the year from a processed header field
 * @type {RegExp}
 */
// const yearProgressRegEx = new RegExp(/^(\d*)_progress$/)

/**
 * Transforms the entire dataset as needed
 * @param  {Object} results The entire results structure. Data is in `results.data`
 * @return {Object}         The transformed results structure
 */
const performResultsTransform = results => {
  const companies = [];
  let minYear = 10000
  let maxYear = 0
  results.data.forEach(item => {
    minYear = Math.min(minYear, +item.year)
    maxYear = Math.max(maxYear, +item.year)
    // Warn about unexpected egg types
    const eggType = EGG_TYPES.find(e => e.name === item.egg_type)
    if(!eggType && item.egg_type !== 'NA') {
      console.warn(`Unexpected egg type for ${item.company}: ${item.egg_type}`)
    }
    const progress = {
      year: +item.year,
      value: isNaN(item.progress) ? item.progress : +Number((+item.progress + Number.EPSILON) * 100).toFixed(1),
      timeline: item.timeline,
      notes: item.reporting_notes
    }
    // Overwrite progress now that we pulled out the numbers
    item.progress = {}
    item.sector = [item.sector]
    const geographies = item.countries !== '' ? item.countries.split(',')
      .map(c => {
        const country = iso.whereCountry(c.trim()) // Object.keys(country_names).find(k => country_names[k] === c.trim())

        if(country === undefined && ['Global','US','EU'].indexOf(c) === -1) {
          // Hard-code swaps
          switch(c) {
            case 'United Kingdom': return 'UK'
            case 'Russia': return 'RUS'
          }
          console.warn(`Unable to find ISO code for country ${c} in company ${item.company}`);
          return c.trim()
        }
        return country ? country.alpha3 : c.trim()
      }) : [item.region]
    geographies.forEach(g => {
      if(g in item.progress) {
        if(item.egg_type in item.progres[g]) {
          item.progress[g][item.egg_type].progress.push(progress)
        } else {
          item.progress[g][item.egg_type] = {
            progress: [progress]
          }
        }
      } else {
        item.progress[g] = _.merge(item.progress[g], {
          [item.egg_type]: {
            progress: [progress]
          }
        })
      }
    })
    delete item.egg_type
    delete item.countries
    delete item.region
    delete item.timeline
    delete item.reporting_notes

    // Find this company if it already exists (this might be a new year or new egg type commitment)
    const companyIdx = companies.findIndex(d => d.company.toLowerCase() === item.company.toLowerCase())
    if(companyIdx !== -1) {
      const company = companies[companyIdx]
      // _.merge isn't deep enough and doesn't seem to handle arrays, so we have to do it manually
      Object.keys(item.progress).forEach(country => {
        if(!(country in company.progress)) {
          company.progress = _.merge(company.progress, item.progress)
        } else {
          Object.keys(item.progress[country]).forEach(eggType => {
            if(!(eggType in company.progress[country])) {
              company.progress[country] = _.merge(company.progress[country], item.progress[country])
            } else {
              // Check timeline and notes for inconsistencies
              /*
              if(company.progress[country][eggType].notes !== '' &&
                company.progress[country][eggType].notes !== item.progress[country][eggType].notes &&) {
                console.warn(`Inconsistent notes for ${company.company}[${country}][${eggType}]. Was ${company.progress[country][eggType].notes}. Now ${item.progress[country][eggType].notes}`)
              }
              if(company.progress[country][eggType].timeline !== '' &&
                company.progress[country][eggType].timeline !== item.progress[country][eggType].timeline) {
                console.warn(`Inconsistent timeline for ${company.company}[${country}][${eggType}]. Was ${company.progress[country][eggType].timeline}. Now ${item.progress[country][eggType].timeline}`)
              }
              */
              company.progress[country][eggType].progress = company.progress[country][eggType].progress.concat(item.progress[country][eggType].progress)
            }
          })
        }
      })
      company.sector = _.uniq(company.sector.concat(item.sector))
      // Set the company min and max year (not the global one)
      company.minYear = Math.min(company.minYear, progress.year)
      company.maxYear = Math.max(company.maxYear, progress.year)

      if(item.additional_details.trim() !== '') {
        company.additional_details.push({year: +item.year, details: item.additional_details.trim()})
      }
      if(item.good_egg_award.trim() !== '') {
        company.good_egg_award = item.good_egg_award.trim()
      }

      if(item.parent.toLowerCase() != company.parent.toLowerCase()) {
        console.warn(`Found inconsistent parenting in ${company.company}. Previously: ${company.parent}, now ${item.parent}`);
      }
      if(company.company_size === '') {
        company.company_size = item.company_size.trim()
      } else if(company.company_size.trim() !== item.company_size.trim() && item.company_size.trim() !== '') {
        console.warn(`Found inconsistent company sizing for ${company.company}. Was ${company.company_size} now ${item.company_size}`)
        company.company_size = item.company_size.trim()
      }
      company.progress_badge[+item.year] = item.progress_badge.trim()
      /*
      if(item.progress_badge !== undefined && item.progress_badge !== '') {
        item.progress_badge = item.progress_badge.trim()
        if(company.progress_badge === '') {
          company.progress_badge = item.progress_badge
        } else if(company.progress_badge !== item.progress_badge) {
          console.warn(`Found inconsistent progress badge for ${company.company}. Was ${company.progress_badge} now ${item.progress_badge}`)
          company.progress_badge = item.progress_badge
        }
      }
      */
    } else {
      // New company
      const company = {
        ...item,
        children: [],
        additional_details: [],
        progress: item.progress,
        minYear: +item.year,
        maxYear: +item.year,
        progress_badge: {[+item.year]: item.progress_badge.trim()}
      }
      if(item.additional_details.trim() !== '') {
        company.additional_details.push({
          year: +item.year,
          details: item.additional_details.trim()
        })
      }
      companies.push(company)
    }
  })

  // Break apart the company names that have multiple companies in it, duplicate the entries entirely (yuck, but, as designed)
  // UNDONE
  /*
  const namesToRemove = []
  companies.forEach(company => {
    const names = company.company.split(',')
    if(names.length > 1) {
      console.debug(`Splitting ${company.company}`);
      namesToRemove.push(company.company)
      names.forEach(name => {
        companies.push({...company, company: name.trim()})
      })
    }
  })
  _.remove(companies, c => namesToRemove.indexOf(c.company) !== -1)
  */

  // Start finding children for the parents, also sort the data descending by year
  companies.forEach(company => {
    // Sort the progress by year and build a progress index
    for(const region in company.progress) {
      for(const eggType in company.progress[region]) {
        company.progress[region][eggType].progress.sort((a, b) => +b.year - +a.year)
      }

      // Sort egg types by EGG_TYPES
      company.progress[region] = Object.keys(company.progress[region])
      .sort((a, b) => {
        const eggTypes = EGG_TYPES.map(e => e.name.toLowerCase())
        return eggTypes.indexOf(a.toLowerCase()) - eggTypes.indexOf(b.toLowerCase())
      })
      .reduce((r, k) => (r[k] = company.progress[region][k], r), {})
    }

    /*
    Sorry..... Basically this just pulls all the values out of the progress
    object and average them by year, replacing non-numbers with 0
    */
    const getProgressesForCompany = company => {
      return _(company.progress).mapValues(v => Object.values(v))
        .values().flatten()
        .map(v => v.progress).flatten()
        .groupBy('year')
        .mapValues(v => v.map(p => p.value))
        .value()
    }
    const progresses = getProgressesForCompany(company)
    for(const year in progresses) {
      progresses[year] = _.mean(progresses[year].map(v => isNaN(v) ? 0 : v))
    }
    // Factor in the child progresses
    _(companies).filter(c => c.parent === company.company)
    .each(child => {
      const childProgresses = getProgressesForCompany(child)
      for(const year in childProgresses) {
        progresses[year] = (progresses[year] + _.mean(childProgresses[year].map(v => isNaN(v) ? 0 : v))) / 2
      }
    })
    company.progress_index = progresses // We sort on this when the progress badges match

    if(company.parent !== '') {
      // Find the parent, add this as a child, only if the child isn't trying to reference itself
      const idx = companies.findIndex(c => c.company.toLowerCase() === company.parent.toLowerCase() &&
        c.company.toLowerCase() !== company.company.toLowerCase())
      if(idx === -1) {
        console.warn(`Unable to find company named ${company.parent} during parent lookup to child ${company.company}`)
      } else {
        companies[idx].children.push(company.company)
      }
    }
  })
  return {companies, minYear, maxYear}
}

/**
 * Loads the EggTrack data from the supplied path
 * @param  {String} path The path to the CSV data
 * @return {Promise}     Promise that resolves with the results of the parse or rejects with the error
 */
export function loadData(path = 'data/egg_track.csv') {
  return new Promise((resolve, reject) => {
    Papa.parse(path, {
      download: true,
      header: true,
      skipEmptyLines: 'greedy',
      error: error => reject(error),
      complete: results => {
        // Transform the results as the app needs it
        const {companies, minYear, maxYear} = performResultsTransform(results)
        // Get the company logos
        axios.get(`${process.env.VUE_APP_ASSETS_PATH || ''}data/company_logos.json`)
        .then(logos => {
          companies.forEach(r => {
            // Lookup logos
            if(r.company in logos.data) {
              try {
                r.logo = require(`./assets/${logos.data[r.company]}`)
              } catch (e) {
                console.error(`Can't find logo from company_logos.json: ${e}`)
              }
            } else {
              // r.logo = logos.data.default
              // Default logo is a slugified version of the company name
              const logoName = slugify(r.company, { lower: true, remove: /['.,-]/g })
              // r.logo = [`${LOGO_ROOT}${logoName}.svg`,`${LOGO_ROOT}${logoName}.png`]
              try {
                r.logo = require(`./assets/img/logos/${logoName}.svg`)
              } catch {
                // Ok try png
                try {
                  r.logo = require(`./assets/img/logos/${logoName}.png`)
                } catch (e) {
                  console.error(`Can't find logo for company ${r.company}: ${e}`)
                }
              }
            }
          })
          resolve({companies, minYear, maxYear})
        }).catch(e => reject(e))
      }
    })
  });
}

/**
 * Checks if a company is a child company of another
 * @param  {Object}  company The company object to check
 * @return {Boolean}         True if the company is a child, false if the company is not
 */
export const isChildCompany = company => { return ['',null].indexOf(company.parent) === -1 }
