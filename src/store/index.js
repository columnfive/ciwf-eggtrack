import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import * as escapeStringRegex from 'escape-string-regexp'

import { loadData } from '../util'

import {
  SELECT_REGION,
  SELECT_SECTOR,
  SET_SEARCH_STRING,
  SET_EGGTRACK_DATA,
  SELECT_COMPANY_SIZE,
  SET_DETAIL_COMPANY,
  SET_SORT_BY
} from './mutations'

import {
  isChildCompany,
  COMPANY_SIZE_OPTIONS,
  REGION_OPTIONS,
  SORT_OPTIONS,
  STATUS_BADGES
} from '../util'

Vue.use(Vuex)

const visibilityFilter = (state, company) => {
  return company.scope === state.selectedRegion &&
  (state.selectedCompanySize !== COMPANY_SIZE_OPTIONS[0] ? company.company_size === state.selectedCompanySize : true) &&
  company.sector.indexOf(state.selectedSector) !== -1 &&
  company.maxYear === state.maxYear &&
  !isChildCompany(company)
}

const applySorting = (state, a, b) => {
  const badgeOrder = Object.keys(STATUS_BADGES)
  if(state.sortBy === 'Most Progress') {
    const badgeDifference = badgeOrder.indexOf(a.progress_badge[state.maxYear].toLowerCase()) -
      badgeOrder.indexOf(b.progress_badge[state.maxYear].toLowerCase())
    if(badgeDifference !== 0 &&
      !(a.progress_badge[state.maxYear] === '' || b.progress_badge[state.maxYear] === ''))
      return badgeDifference;
    return b.progress_index[state.maxYear] - a.progress_index[state.maxYear]
  } else if(state.sortBy === 'Least Progress') {
    const badgeDifference = badgeOrder.indexOf(b.progress_badge[state.maxYear].toLowerCase()) -
      badgeOrder.indexOf(a.progress_badge[state.maxYear].toLowerCase())
    if(badgeDifference !== 0
      && !(a.progress_badge[state.maxYear] === '' || b.progress_badge[state.maxYear] === ''))
      return badgeDifference;
    return a.progress_index[state.maxYear] - b.progress_index[state.maxYear]
  }
  return 0
}

const updateVisibleCompanies = _.debounce((state, searchRegex) => {
  if(searchRegex) {
    state.visibleCompanies = state.availableCompanies
      .filter(c => c.company.match(searchRegex))
      .sort((a, b) => applySorting(state, a, b))
  } else {
    state.visibleCompanies = state.availableCompanies
      .filter(c => visibilityFilter(state, c))
      .sort((a, b) => applySorting(state, a, b))
  }
  if(state.pdf) {
    state.visibleCompanies = _.sortBy(state.visibleCompanies, [c => c.company.toLowerCase()])
    state.visibleCompanies.forEach(company => {
      company.children = _.sortBy(company.children, [c => c.toLowerCase()])
    })
  }
}, 500, { leading: true, trailing: false })

export default new Vuex.Store({
  state: {
    availableRegions: [],
    selectedRegion: null,
    searchString: '',
    availableSectors: [],
    selectedSector: null,
    availableSortOptions: SORT_OPTIONS,
    sortBy: SORT_OPTIONS[0],
    selectedCompanySize: COMPANY_SIZE_OPTIONS[0],
    selectedEggTypes: [],
    availableCompanies: [],
    visibleCompanies: [],
    searching: false,
    detailCompany: null,
    maxYear: 0,
    minYear: 0,
    pdf: process.env.VUE_APP_PDF_BUILD ? 1 : 0
  },
  mutations: {
    [SET_DETAIL_COMPANY](state, payload) {
      state.detailCompany = payload
    },
    [SELECT_COMPANY_SIZE](state, payload) {
      state.selectedCompanySize = payload
      updateVisibleCompanies(state)
      // state.visibleCompanies = _.sortBy(state.visibleCompanies, ['company'])
    },
    [SELECT_REGION](state, payload) {
      state.selectedRegion = payload
      updateVisibleCompanies(state)
      // state.visibleCompanies = _.sortBy(state.visibleCompanies, ['company'])
    },
    [SELECT_SECTOR](state, payload) {
      state.selectedSector = payload
      updateVisibleCompanies(state)
      // state.visibleCompanies = _.sortBy(state.visibleCompanies, ['company'])
    },
    [SET_SEARCH_STRING](state, payload) {
      state.searchString = payload
      state.searching = payload !== ''
      const searchRegex = new RegExp(escapeStringRegex(payload), 'i')
      if(state.searching) {
        updateVisibleCompanies(state, searchRegex)
      } else {
        updateVisibleCompanies(state)
      }
    },
    [SET_EGGTRACK_DATA](state, payload) {
      // Now that we have data, set up some of our option variables
      const data = _(payload.companies)
      state.availableRegions = REGION_OPTIONS.map(r => r.name) // data.map(d => d.scope).uniq().value()
      state.availableSectors = _(data).map(d => d.sector).flatten().uniq().sort().value()
      state.availableCompanies = payload.companies

      // Set up default selections
      state.selectedRegion = state.availableRegions[0]
      state.selectedSector = state.availableSectors[0]

      state.maxYear = payload.maxYear
      state.minYear = payload.minYear

      // Set the available companies to the companies filtered by region and industry
      updateVisibleCompanies(state)
      // state.visibleCompanies = _.sortBy(state.visibleCompanies, ['company'])
    },
    [SET_SORT_BY](state, payload) {
      state.sortBy = payload
      updateVisibleCompanies(state)
    }
  },
  actions: {
    async fetchEggtrackData({ commit }) {
      commit(SET_EGGTRACK_DATA, await loadData(`${process.env.VUE_APP_ASSETS_PATH || ''}data/egg_track.csv`))
    },
    showDetailModal({commit}, company) {
      commit(SET_DETAIL_COMPANY, company)
    },
    hideDetailModal({commit}) {
      commit(SET_DETAIL_COMPANY, null)
    }
  },
  modules: {
  }
})
