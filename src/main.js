import Vue from 'vue'
import App from './App.vue'
import PDF from './PDF.vue'
import store from './store'
import { VBTooltip } from 'bootstrap-vue'
import * as am4core from '@amcharts/amcharts4/core'
import i18n from './i18n'

am4core.addLicense("CH243188005");

Vue.config.productionTip = false
Vue.directive('b-tooltip', VBTooltip)

if(!process.env.VUE_APP_PDF_BUILD) {
new Vue({
  store,
  i18n,
  render: h => h(App)
}).$mount('#eggtrack')
} else {
  new Vue({
    store,
    i18n,
    render: h => h(PDF)
  }).$mount('#pdf')
}
//
// new Vue({
//   store,
//   render: h => h(PDF)
// }).$mount('#pdf')
